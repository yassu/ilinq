#!/usr/bin/env python
# -*- coding: utf-8 -*-

from invoke import task


def _run_commands(ctx, commands):
    for command in commands:
        print(command)
        ctx.run(command)


@task
def test(ctx, verbose=False):
    """ run flake8 and tests """
    ctx.run('flake8')

    ctx.run('nosetests --verbose')


@task
def build_doc(ctx):
    """ build sphinx docs """
    ctx.run('cd docs/ && make html')


@task
def clean_doc(ctx):
    """
    delete junk files for doc
    """
    ctx.run('cd docs/ && make clean')


@task
def view_doc(ctx):
    """ open readme of sphinx docs"""
    ctx.run("open -a 'Google Chrome' docs/_build/html/index.html")


@task
def release(ctx):
    """
    release this project
    """
    from ilinq import __VERSION__
    _run_commands(
        ctx, [
            'python setup.py sdist bdist_wheel', 'twine upload dist/*',
            'rm -rf dist/*', 'git tag {}'.format(__VERSION__)
        ])
